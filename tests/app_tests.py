from app import app
from base_62_converter import saturate, dehydrate
import unittest


class ShortyBaseTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        app.config['SECRET_KEY'] = '123'
        self.app = app.test_client()
        self.app.testing = True


    def tearDown(self):
        pass

    def test_get_home(self):
        response = self.app.get('/')
        self.assertEqual(response.status_code, 200)
        
    def test_get_add(self):
        response = self.app.get('/add')
        self.assertEqual(response.status_code, 200)

    def test_dehydrate(self):
        id = 12345
        self.assertEqual(dehydrate(id), "3D7")

    def test_saturate(self):
        url = "3D7"
        self.assertEqual(saturate(url), 12345)