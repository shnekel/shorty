# shorty
Link shortener. Flask training project
# Install
```
pip install -r requirements.txt
```
# Database
In this project I used SQLite3.
Database name is defined in `./db.py`
## Schema
```
sqlite> .schema
create table links (
id integer primary key,
url string,    
short_url string,    
unique(short_url)); 
```
# Run
```
export SECRET_KEY=<secret key here>
FLASK_APP=app.py flask run
```
