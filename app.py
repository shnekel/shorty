from urllib.parse import urlparse

from flask import Flask, redirect, render_template, request, session, url_for
from os import environ
from base_62_converter import dehydrate, saturate
from db import get_db, query_db

INSERT_QUERY = "INSERT INTO links (url, short_url) VALUES (?, ?)"

app = Flask(__name__)
app.config["SECRET_KEY"] = environ.get("FLASK_SECRET", None)

@app.route("/")
def index():
    return render_template("index.html"), 200


@app.route("/<short_id>")
def go_to_url(short_id):
    idx = saturate(short_id)
    with app.app_context():
        res = query_db(
            "SELECT url from links where id=?",
            [idx],
            one=True
        )
        full_url = None
        if res:
            full_url = res["url"]
        else:
            return "404 not found", 404

    return redirect(full_url)


@app.route("/add", methods=["POST", "GET"])
def add_line():
    if request.method == "POST":
        url = request.form.get("url", "empty")
        short_url = None
        with app.app_context():
            db = get_db()
            cursor = db.cursor()
            cursor.execute(INSERT_QUERY, (url, f"{url}-short"))
            db.commit()
            res = query_db(
                "SELECT id from links where url=?",
                (url,),
                one=True
            )
            idx = res["id"]
            short_url = dehydrate(idx)
            cursor.execute(
                "UPDATE links SET short_url=? where id=?",
                (short_url, idx)
            )
            db.commit()
            session["short_url"] = short_url
        return redirect(url_for("success_redirect"))

    return "OK"

@app.route("/ok")
def success_redirect():
    short_url = f"{request.url_root}{session.get('short_url', None)}"
    return render_template(
        "ok.html", 
        short_url=short_url
    )
